from django.apps import AppConfig


class ChatikConfig(AppConfig):
    name = 'chatik'
